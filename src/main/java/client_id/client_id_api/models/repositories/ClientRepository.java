package client_id.client_id_api.models.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import client_id.client_id_api.models.entities.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
  @Query("SELECT c.state AS state, COUNT(c) AS count FROM Client c GROUP BY c.state")
  List<Object[]> getClientCountByState();

  @Query("SELECT c FROM Client c WHERE c.sales_id = ?1")
  List<Client> findBySalesId(Long sales_id);

}
