package client_id.client_id_api.models.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import client_id.client_id_api.models.entities.Client;
import client_id.client_id_api.models.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  Optional<User> findByUsername(String username);

  Boolean existsByUsername(String username);

  long countByRoleNotAndDeletedFalse(String role);

  @Query("SELECT u, COUNT(c.id) as clientCount " +
      "FROM User u LEFT JOIN Client c ON u.id = c.sales_id " +
      "WHERE u.deleted = false AND u.role != 'Admin' " +
      "GROUP BY u.id " +
      "ORDER BY clientCount DESC")
  List<Object[]> findTop5UsersByClientCount();
}
