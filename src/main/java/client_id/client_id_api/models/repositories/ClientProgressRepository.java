package client_id.client_id_api.models.repositories;

import client_id.client_id_api.models.entities.ClientProgress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientProgressRepository extends JpaRepository<ClientProgress, Long> {
  List<ClientProgress> findByClientIdOrderByIdDesc(Long clientId);
}
