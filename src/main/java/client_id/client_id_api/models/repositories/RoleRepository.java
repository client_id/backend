package client_id.client_id_api.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import client_id.client_id_api.models.entities.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
  Optional<Role> findByName(String name);
}