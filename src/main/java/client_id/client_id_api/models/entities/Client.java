package client_id.client_id_api.models.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "clients")
public class Client {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;
  private String name;
  private Integer sales_id;
  private String position;
  private String company;
  private String industry_type;
  private String phone;
  private String email;
  private String source_lit;
  private String state;

  @ManyToOne
  @JoinColumn(name = "sales_id", referencedColumnName = "id", insertable = false, updatable = false)
  private User salesPerson;
}