package client_id.client_id_api.dtos;

import client_id.client_id_api.models.entities.Client;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientDto {
  private Long id;
  private String name;
  private String position;
  private String company;
  private String industry_type;
  private String phone;
  private String email;
  private String source_lit;
  private String state;
  private Integer sales_id;
  private String sales_name;

  public ClientDto(Client client) {
    this.id = client.getId();
    this.name = client.getName();
    this.position = client.getPosition();
    this.company = client.getCompany();
    this.industry_type = client.getIndustry_type();
    this.phone = client.getPhone();
    this.email = client.getEmail();
    this.source_lit = client.getSource_lit();
    this.state = client.getState();
    this.sales_id = client.getSales_id();
    if (client.getSalesPerson() != null) {
      this.sales_name = client.getSalesPerson().getUsername();
    }
  }
}
