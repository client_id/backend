package client_id.client_id_api.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegisterUserDto {
  private String username;
  private String password;
  private String role;
}
