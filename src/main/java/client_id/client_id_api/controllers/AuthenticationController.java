package client_id.client_id_api.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import client_id.client_id_api.dtos.LoginUserDto;
import client_id.client_id_api.dtos.RegisterUserDto;
import client_id.client_id_api.models.entities.User;
import client_id.client_id_api.services.AuthenticationService;
import client_id.client_id_api.services.JwtService;
import lombok.Getter;
import lombok.Setter;

@RequestMapping("/auth")
@RestController
public class AuthenticationController {
  private final JwtService jwtService;

  private final AuthenticationService authenticationService;

  public AuthenticationController(JwtService jwtService, AuthenticationService authenticationService) {
    this.jwtService = jwtService;
    this.authenticationService = authenticationService;
  }

  @PostMapping("/signup")
  public ResponseEntity<User> register(@RequestBody RegisterUserDto registerUserDto) {
    User registeredUser = authenticationService.signup(registerUserDto);

    return ResponseEntity.ok(registeredUser);
  }

  @PostMapping("/login")
  public ResponseEntity<LoginResponse> authenticate(@RequestBody LoginUserDto loginUserDto) {
    User authenticatedUser = authenticationService.authenticate(loginUserDto);

    String jwtToken = jwtService.generateToken(authenticatedUser);

    LoginResponse loginResponse = new LoginResponse();

    loginResponse.setToken(jwtToken);
    loginResponse.setId(authenticatedUser.getId());
    loginResponse.setUsername(authenticatedUser.getUsername());
    loginResponse.setRole(authenticatedUser.getRole());
    loginResponse.setExpiresIn(jwtService.getExpirationTime());

    return ResponseEntity.ok(loginResponse);
  }

  @Getter
  @Setter
  public class LoginResponse {
    private String token;
    private long id;
    private String username;
    private String role;
    private long expiresIn;
  }

}
