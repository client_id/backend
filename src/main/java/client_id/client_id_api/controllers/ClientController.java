
package client_id.client_id_api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import client_id.client_id_api.dtos.ClientDto;
import client_id.client_id_api.models.entities.Client;
import client_id.client_id_api.models.entities.ClientProgress;
import client_id.client_id_api.services.ClientService;
import client_id.client_id_api.services.UserService;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/clients")
public class ClientController {

  @Autowired
  private ClientService clientService;

  @Autowired
  private UserService userService;

  @GetMapping
  public List<ClientDto> getAllClients() {
    return clientService.getAllClients();
  }

  @GetMapping("/{id}")
  public Client getClientById(@PathVariable Long id) {
    return clientService.getClientById(id);
  }

  @PostMapping
  public Client createClient(@RequestBody Client client) {
    return clientService.saveClient(client);
  }

  @PutMapping("/{id}")
  public Client updateClient(@PathVariable Long id, @RequestBody Client client) {
    Client existingClient = clientService.getClientById(id);
    if (existingClient != null) {
      return existingClient;
    }
    return null;
  }

  @DeleteMapping("/{id}")
  public void deleteClient(@PathVariable Long id) {
    clientService.deleteClient(id);
  }

  @GetMapping("/{id}/progress")
  public List<ClientProgress> getClientProgress(@PathVariable Long id) {
    return clientService.getClientProgressByClientId(id);
  }

  @PutMapping("/{id}/state")
  public void updateClientState(@PathVariable Long id, @RequestBody StateUpdateRequest request) {
    clientService.updateClientState(id, request.getState(), request.getNotes(), request.getDate());
  }

  @Getter
  @Setter
  public static class StateUpdateRequest {
    private Date date;
    private String state;
    private String notes;
  }

  @GetMapping("/state-count")
  public Map<String, Long> getClientCountByState() {
    return clientService.getClientCountByState();
  }

  @GetMapping("/user-client-counts")
  public Map<String, Long> getUserAndClientCounts() {
    Map<String, Long> counts = new HashMap<>();
    counts.put("client", clientService.getClientCount());
    counts.put("user", userService.getUserCountExcludingAdmin());
    return counts;
  }

  @GetMapping("/state-count/{userId}")
  public Map<String, Long> getClientCountByState(@PathVariable Long userId) {
    return clientService.getClientCountByStateAndId(userId);
  }

  @GetMapping("/user-client-counts/{userId}")
  public Map<String, Long> getUserAndClientCounts(@PathVariable Long userId) {
    Map<String, Long> counts = new HashMap<>();
    counts.put("client", clientService.getClientCount(userId));
    counts.put("user", userService.getUserCountExcludingAdmin());
    return counts;
  }

}
