package client_id.client_id_api.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import client_id.client_id_api.models.entities.User;
import client_id.client_id_api.services.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequestMapping("/users")
@RestController
public class UserController {
  private final UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/me")
  public ResponseEntity<User> authenticatedUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

    User currentUser = (User) authentication.getPrincipal();

    return ResponseEntity.ok(currentUser);
  }

  @GetMapping
  public ResponseEntity<List<User>> allUsers() {
    List<User> users = userService.allUsers();

    return ResponseEntity.ok(users);
  }

  @PutMapping("/{id}")
  public User updateUser(@PathVariable Long id, @RequestBody User updatedUser) {
    return userService.updateUser(id, updatedUser);
  }

  // Endpoint to soft delete a user
  @DeleteMapping("/{id}")
  public void softDeleteUser(@PathVariable Long id) {
    userService.softDeleteUser(id);
  }

  @GetMapping("/top-users")
  public List<Map<String, Object>> getTop5UsersByClientCount() {
    return userService.getTop5UsersByClientCount().stream().map(result -> {
      User user = (User) result[0];
      Long clientCount = (Long) result[1];
      Map<String, Object> map = new HashMap<>();
      map.put("username", user.getUsername());
      map.put("clientCount", clientCount);
      return map;
    }).collect(Collectors.toList());
  }

}
