package client_id.client_id_api.services;

import org.springframework.stereotype.Service;

import client_id.client_id_api.models.entities.User;
import client_id.client_id_api.models.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {
  private final UserRepository userRepository;

  public UserService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  public List<User> allUsers() {
    List<User> users = new ArrayList<>();

    userRepository.findAll().forEach(users::add);

    return users;
  }

  // Method to update a user
  public User updateUser(Long id, User updatedUser) {
    return userRepository.findById(id).map(user -> {
      user.setUsername(updatedUser.getUsername());
      // Check if the password is not empty
      if (updatedUser.getPassword() != null && !updatedUser.getPassword().isEmpty()) {
        user.setPassword(updatedUser.getPassword());
      }
      user.setRole(updatedUser.getRole());
      return userRepository.save(user);
    }).orElseThrow(() -> new RuntimeException("User not found"));
  }

  // Method to soft delete a user
  public void softDeleteUser(Long id) {
    userRepository.findById(id).map(user -> {
      user.setDeleted(true);
      return userRepository.save(user);
    }).orElseThrow(() -> new RuntimeException("User not found"));
  }

  public long getUserCountExcludingAdmin() {
    return userRepository.countByRoleNotAndDeletedFalse("admin");
  }

  public List<Object[]> getTop5UsersByClientCount() {
    return userRepository.findTop5UsersByClientCount();
  }

  // public Long getUserCountExcludingAdmin() {
  // return userRepository.findAll().stream()
  // .filter(user -> !"admin".equals(user.getRole()) && !user.isSoftDeleted())
  // .count();
  // }

  // // Get the total count of all users
  // public Long getUserCount() {
  // return userRepository.count();
  // }

}
