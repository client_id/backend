package client_id.client_id_api.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import client_id.client_id_api.dtos.ClientDto;
import client_id.client_id_api.models.entities.Client;
import client_id.client_id_api.models.entities.ClientProgress;
import client_id.client_id_api.models.repositories.ClientProgressRepository;
import client_id.client_id_api.models.repositories.ClientRepository;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import java.util.stream.Collectors;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ClientProgressRepository clientProgressRepository;

    public List<ClientDto> getAllClients() {
        List<Client> clients = clientRepository.findAll();
        return clients.stream()
                .map(ClientDto::new)
                .collect(Collectors.toList());

    }

    public Client getClientById(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    public Client saveClient(Client client) {
        return clientRepository.save(client);
    }

    public void deleteClient(Long id) {
        List<ClientProgress> clientProgresses = clientProgressRepository.findByClientIdOrderByIdDesc(id);
        clientProgressRepository.deleteAll(clientProgresses);

        // Delete the client
        clientRepository.deleteById(id);
    }

    public List<ClientProgress> getClientProgressByClientId(Long clientId) {
        return clientProgressRepository.findByClientIdOrderByIdDesc(clientId);
    }

    @Transactional
    public void updateClientState(Long clientId, String state, String notes, Date date) {
        Client client = clientRepository.findById(clientId)
                .orElseThrow(() -> new RuntimeException("Client not found"));

        client.setState(state);
        clientRepository.save(client);

        ClientProgress clientProgress = new ClientProgress();
        clientProgress.setDate(date);
        clientProgress.setState(state);
        clientProgress.setNotes(notes);
        clientProgress.setClient(client);

        clientProgressRepository.save(clientProgress);
    }

    public Map<String, Long> getClientCountByState() {
        List<Object[]> results = clientRepository.getClientCountByState();
        Map<String, Long> clientCountByState = new HashMap<>();
        for (Object[] result : results) {
            String state = (String) result[0];
            Long count = (Long) result[1];
            clientCountByState.put(state, count);
        }
        return clientCountByState;
    }

    public long getClientCount() {
        return clientRepository.count();
    }

    // ------------

    public Map<String, Long> getClientCountByStateAndId(Long userId) {
        return clientRepository.findBySalesId(userId).stream()
                .collect(Collectors.groupingBy(Client::getState, Collectors.counting()));
    }

    // Get the total count of clients for a specific user
    public Long getClientCount(Long userId) {
        return (long) clientRepository.findBySalesId(userId).size();
    }

    // Get the total count of all clients
    public Long getClientCountById() {
        return clientRepository.count();
    }

}
